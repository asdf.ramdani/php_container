FROM php:5.6.40-apache-stretch

LABEL MAINTAINER="Saepulloh Ramdani<saepulloh.ramndia@gmail.com>"

# Update OS packages and install required ones.
RUN apt-get update && apt-get install -y \ 
        curl \
        libmagickwand-dev  \
        libcurl4-openssl-dev \
        libmemcached-dev \
        libjpeg62-turbo-dev \
        libz-dev \
        libonig-dev \ 
        libpq-dev \
        libjpeg-dev \
        libpng-dev \
        libfreetype6-dev \
        libssl-dev \
        libmcrypt-dev \
        libicu-dev \
        icu-devtools \
        zip \
        unzip \
        build-essential \
        libaio1 \
        libzip-dev \
        wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && rm /var/log/lastlog /var/log/faillog

#
#--------------------------------------------------------------------------
# Application Dependencies
#--------------------------------------------------------------------------
#

# Install OCI8 & instanclient
ADD instantclient-basic-linux.x64-12.2.0.1.0.tar.gz /usr/local
ADD instantclient-sdk-linux.x64-12.2.0.1.0.tar.gz /usr/local
ADD instantclient-sqlplus-linux.x64-12.2.0.1.0.tar.gz /usr/local

RUN ln -s /usr/local/instantclient_12_2 /usr/local/instantclient \
  && ln -s /usr/local/instantclient/libclntsh.so.* /usr/local/instantclient/libclntsh.so \
  && ln -s /usr/local/instantclient/lib* /usr/lib \
  && ln -s /usr/local/instantclient/sqlplus /usr/bin/sqlplus \
  && chmod 755 -R /usr/local/instantclient

# are not included in default image. Also install our compiled oci8 extensions.
RUN docker-php-ext-configure oci8 --with-oci8=instantclient,/usr/local/instantclient \
    && docker-php-ext-install \
        oci8 \
    # && docker-php-ext-configure pdo_oci --with-pdo-oci=instantclient,/usr/lib/oracle/instantclient,12_1 \
    # && docker-php-ext-install \
    #     pdo_oci \
    && docker-php-ext-install zip  \
        bcmath \
        curl \
        exif \
        intl \
        mbstring \
        opcache \
        pdo_mysql \
        pgsql pdo_pgsql \
        pcntl \
        tokenizer \
        xml \
    # Install the PHP gd library
    && docker-php-ext-configure gd \
        --with-jpeg=/usr/lib \
        --with-jpeg=/usr/include \
        --with-freetype=/usr/include/freetype2 && \
        docker-php-ext-install gd

# RUN pecl install memcached
# RUN echo extension=memcached.so >> /usr/local/etc/php/conf.d/memcached.ini

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Copy Config
COPY config/php.ini /usr/local/etc/php/conf.d/app.ini
COPY config/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY config/start-apache /usr/local/bin
RUN a2enmod rewrite

# PHP Error Log Files
RUN mkdir /var/log/php
RUN touch /var/log/php/errors.log && chmod 777 /var/log/php/errors.log

# Copy application source
# RUN mkdir /var/www/html
COPY ./html/index.php /var/www/html
RUN chown -R www-data:www-data /var/www

CMD [ "start-apache" ]
